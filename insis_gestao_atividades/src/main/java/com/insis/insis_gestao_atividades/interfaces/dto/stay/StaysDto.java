package com.insis.insis_gestao_atividades.interfaces.dto.stay;

import com.insis.insis_gestao_atividades.interfaces.dto.activity.ActivityDto;

import java.util.List;

public class StaysDto {
    public List<String> alojamentos;

    public StaysDto(List<String> activities) {
        this.alojamentos = activities;
    }
}
