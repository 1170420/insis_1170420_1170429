package com.insis.insis_gestao_atividades.interfaces.facade.other_service;


import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServiceDto;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServicesDto;

public interface OtherServiceFacade {

    OtherServiceDto createOtherService(OtherServiceDto otherServiceDto);

    OtherServiceDto updateOtherService(OtherServiceDto otherServiceDto);

    void deleteOtherService(int id);

    PayloadDto<OtherServicesDto> getOtherservices();

}
