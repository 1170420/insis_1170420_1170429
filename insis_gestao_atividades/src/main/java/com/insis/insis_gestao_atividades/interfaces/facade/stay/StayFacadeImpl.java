package com.insis.insis_gestao_atividades.interfaces.facade.stay;

import com.insis.insis_gestao_atividades.application.services.stay.StayService;
import com.insis.insis_gestao_atividades.domain.models.stay.Stay;
import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.stay.StayDto;
import com.insis.insis_gestao_atividades.interfaces.dto.stay.StaysDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StayFacadeImpl implements StayFacade {

    @Autowired
    private StayService stayService;

    @Override
    public StayDto createStay(StayDto stayDto) {
        Stay createdStay = stayService.createStay(new Stay(stayDto.description, stayDto.price));
        return new StayDto(createdStay.getDescription(), createdStay.getPrice());
    }

    @Override
    public StayDto updateStay(StayDto stayDto) {
        Stay createdStay = stayService.updateStay(new Stay(stayDto.description, stayDto.price));
        return new StayDto(createdStay.getDescription(), createdStay.getPrice());
    }

    @Override
    public void deleteStay(int id) {
        stayService.deleteStay(id);
    }

    @Override
    public PayloadDto<StaysDto> getStays() {
        return new PayloadDto<>(stayService.getStays());
    }
}
