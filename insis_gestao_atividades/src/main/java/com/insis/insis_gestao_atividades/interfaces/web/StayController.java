package com.insis.insis_gestao_atividades.interfaces.web;

import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServiceDto;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServicesDto;
import com.insis.insis_gestao_atividades.interfaces.dto.stay.StayDto;
import com.insis.insis_gestao_atividades.interfaces.dto.stay.StaysDto;
import com.insis.insis_gestao_atividades.interfaces.facade.stay.StayFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/servicos/alojamentos")
public class StayController {

    @Autowired
    private StayFacade stayFacade;

    @GetMapping("/obterAlojamentos")
    public PayloadDto<StaysDto> getAll() {
        return stayFacade.getStays();
    }

    @PostMapping("/criarAlojamento")
    public StayDto addStay(@RequestBody StayDto stay) {
        return stayFacade.createStay(stay);
    }

    @PutMapping("/atualizarAlojamento")
    public StayDto updateStay(@RequestBody StayDto stay) {
        return stayFacade.updateStay(stay);
    }

    @DeleteMapping("/apagarAlojamento/{id}")
    public void deleteOutrosServicos(@PathVariable int id) {
        stayFacade.deleteStay(id);
    }
}
