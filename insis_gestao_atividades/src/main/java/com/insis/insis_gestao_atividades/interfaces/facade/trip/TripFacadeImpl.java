package com.insis.insis_gestao_atividades.interfaces.facade.trip;


import com.insis.insis_gestao_atividades.application.services.trip.TripService;
import com.insis.insis_gestao_atividades.domain.models.trip.Trip;
import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.trip.TripDto;
import com.insis.insis_gestao_atividades.interfaces.dto.trip.TripsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TripFacadeImpl implements TripFacade {

    @Autowired
    private TripService tripService;

    @Override
    public TripDto createTrip(TripDto tripDto) {
        Trip createdTrip = tripService.createTrip(new Trip(tripDto.description, tripDto.price));
        return new TripDto(createdTrip.getDescription(), createdTrip.getPrice());
    }

    @Override
    public TripDto updateTrip(TripDto tripDto) {
        Trip createdTrip = tripService.updateTrip(new Trip(tripDto.description, tripDto.price));
        return new TripDto(createdTrip.getDescription(), createdTrip.getPrice());
    }

    @Override
    public void deleteTrip(int id) {
        tripService.deleteTrip(id);
    }

    @Override
    public PayloadDto<TripsDto> getTrips() {
        return new PayloadDto<>(tripService.getTrips());
    }
}
