package com.insis.insis_gestao_atividades.interfaces.dto.trip;

public class TripDto {
    public String description;
    public int price;

    public TripDto(String description, int price) {
        this.description = description;
        this.price = price;
    }

    @Override
    public String toString() {
        return description + " - " + price;
    }
}
