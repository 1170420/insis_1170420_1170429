package com.insis.gestao_pagamentos.interfaces.facade.payment;

import com.insis.gestao_pagamentos.interfaces.dto.activity.PaymentDto;
import com.insis.gestao_pagamentos.models.payment.Payment;
import com.insis.gestao_pagamentos.services.payment.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentFacadeImpl implements com.insis.gestao_pagamentos.interfaces.facade.payment.PaymentFacade {

    @Autowired
    private PaymentService paymentService;

    @Override
    public PaymentDto createPayment() {
        Payment paymentCreated = paymentService.createPayment();
        return new PaymentDto(paymentCreated.getResult());
    }

    @Override
    public PaymentDto updatePayment(PaymentDto paymentDto) {
        Payment updatedPayment = paymentService.updatePayment(new Payment(paymentDto.payment));
        return new PaymentDto(updatedPayment.getResult());
    }

    @Override
    public void deletePayment(int id) {
        paymentService.deletePayment(id);
    }

    public List<PaymentDto> getPayments() {
        return paymentService.getPayments();
    }
}
