DROP TABLE IF EXISTS payment;
DROP SEQUENCE IF EXISTS hibernate_sequence;

create table payment (id integer not null, result varchar(255), primary key (id));
create sequence hibernate_sequence start with 1 increment by 1;