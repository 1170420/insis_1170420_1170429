package com.insis.insis_gestao_atividades.application.services.other_services;

import com.insis.insis_gestao_atividades.domain.models.activity.Activity;
import com.insis.insis_gestao_atividades.domain.models.other_service.OtherService;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServicesDto;

public interface OtherServiceService {

    OtherService createOtherService(OtherService otherService);

    OtherService updateOtherService(OtherService OtherService);

    void deleteOtherService(int id);

    OtherServicesDto getOtherServices();
}
