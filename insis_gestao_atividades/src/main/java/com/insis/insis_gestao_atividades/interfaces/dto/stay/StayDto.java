package com.insis.insis_gestao_atividades.interfaces.dto.stay;

public class StayDto {
    public String description;
    public int price;

    public StayDto(String description, int price) {
        this.description = description;
        this.price = price;
    }

    @Override
    public String toString() {
        return description + " - " + price;
    }
}
