package com.insis.insis_gestao_atividades.application.services.stay;

import com.insis.insis_gestao_atividades.domain.models.other_service.OtherService;
import com.insis.insis_gestao_atividades.domain.models.stay.Stay;
import com.insis.insis_gestao_atividades.infrastructure.persistence.StayRepository;
import com.insis.insis_gestao_atividades.interfaces.dto.stay.StaysDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class StayServiceImpl implements StayService {

    @Autowired
    private StayRepository stayRepository;


    @Override
    public Stay createStay(Stay sta) {
        return stayRepository.save(sta);
    }

    @Override
    public Stay updateStay(Stay stay) {
        Stay old;

        Optional<Stay> optionalObj = stayRepository.findAll()
                .stream().filter(a -> a.equals(stay))
                .findFirst();
        if (optionalObj.isPresent()) {
            old = optionalObj.get();
            old.setDescription(stay.getDescription());
            stayRepository.save(old);
        } else {
            return new Stay();
        }
        return old;
    }

    @Override
    public void deleteStay(int id) {
        stayRepository.deleteById(id);
    }

    public StaysDto getStays() {
        List<String> stays = stayRepository.findAll().stream().map(Stay::toString).collect(Collectors.toList());
        return new StaysDto(stays);
    }
}
