package com.insis.insis_gestao_atividades.domain.models.other_service;

import javax.persistence.*;
import java.util.Objects;

@Entity(name="OTHER_SERVICE")
public class OtherService {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "description")
    private String description;

    public OtherService(String description) {
        this.description = description;
    }

    public OtherService() {

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OtherService)) {
            return false;
        }

        final OtherService other = (OtherService) obj;
        return getDescription().equals(other.getDescription());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }

    @Override
    public String toString() {
        return description;
    }
}
