package com.insis.insis_gestao_atividades.interfaces.web;

import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.activity.ActivitiesDto;
import com.insis.insis_gestao_atividades.interfaces.dto.activity.ActivityDto;
import com.insis.insis_gestao_atividades.interfaces.facade.activity.ActivityFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/servicos/atividades")
public class ActivityController {

    @Autowired
    private ActivityFacade activityFacade;

    @GetMapping("/obterAtividades")
    public PayloadDto<ActivitiesDto> getAll() {
        return activityFacade.getActivities();
    }

    @PostMapping("/criarAtividade")
    public ActivityDto addActivity(@RequestBody ActivityDto activity) {
        return activityFacade.createActivity(activity);
    }

    @PutMapping("/atualizarAtividade")
    public ActivityDto updateActivity(@RequestBody ActivityDto activity) {
        return activityFacade.updateActivity(activity);
    }

    @DeleteMapping("/apagarAtividade/{id}")
    public void deleteActivity(@PathVariable int id) {
        activityFacade.deleteActivity(id);
    }
}
