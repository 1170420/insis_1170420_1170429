package com.insis.insis_gestao_atividades.infrastructure.persistence;

import com.insis.insis_gestao_atividades.domain.models.trip.Trip;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.naming.Name;
import javax.persistence.Table;

public interface TripRepository extends JpaRepository<Trip, Integer> {
}
