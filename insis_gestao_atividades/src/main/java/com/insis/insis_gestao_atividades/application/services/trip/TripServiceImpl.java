package com.insis.insis_gestao_atividades.application.services.trip;

import com.insis.insis_gestao_atividades.domain.models.stay.Stay;
import com.insis.insis_gestao_atividades.domain.models.trip.Trip;
import com.insis.insis_gestao_atividades.infrastructure.persistence.TripRepository;
import com.insis.insis_gestao_atividades.interfaces.dto.trip.TripsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class TripServiceImpl implements TripService{

    @Autowired
    private TripRepository tripRepository;

    public Trip createTrip(Trip trip) {
        return tripRepository.save(trip);
    }

    @Override
    public Trip updateTrip(Trip trip) {
        Trip old;

        Optional<Trip> optionalObj = tripRepository.findAll()
                .stream().filter(a -> a.equals(trip))
                .findFirst();
        if (optionalObj.isPresent()) {
            old = optionalObj.get();
            old.setDescription(trip.getDescription());
            tripRepository.save(old);
        } else {
            return new Trip();
        }
        return old;
    }

    @Override
    public void deleteTrip(int id) {
        tripRepository.deleteById(id);
    }

    public TripsDto getTrips() {
        List<String> trips = tripRepository.findAll().stream().map(Trip::toString).collect(Collectors.toList());
        return new TripsDto(trips);
    }
}
