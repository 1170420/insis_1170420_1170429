package com.insis.insis_gestao_atividades.interfaces.dto.trip;

import java.util.List;

public class TripsDto {
    public List<String> viagens;

    public TripsDto(List<String> trips) {
        this.viagens = trips;
    }
}
