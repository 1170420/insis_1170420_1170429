package form.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.TaskListener;

public class SelectedAnswerListener implements TaskListener {

    private static long serialVersionUID = 1L;

    private static final String SELECTED_ANSWER = "selectedAnswer";
    private static final String RESULT = "result";

    public Expression variableNameExpression;

    @Override
    @SuppressWarnings("unchecked")
    public void notify(DelegateTask delegateTask) {
        String selectedAnswer = delegateTask.getVariable(SELECTED_ANSWER, String.class);

        System.out.println("listener string (reservar): " + selectedAnswer);

        if(selectedAnswer != null && selectedAnswer.equals("false")) {
            delegateTask.setVariable(RESULT, "false");
        }
    }
}
