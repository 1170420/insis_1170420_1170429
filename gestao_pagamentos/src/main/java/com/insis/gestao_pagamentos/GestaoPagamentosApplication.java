package com.insis.gestao_pagamentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestaoPagamentosApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestaoPagamentosApplication.class, args);
    }

}
