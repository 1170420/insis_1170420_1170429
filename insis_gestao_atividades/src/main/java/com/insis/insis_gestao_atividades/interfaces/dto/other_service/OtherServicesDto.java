package com.insis.insis_gestao_atividades.interfaces.dto.other_service;

import java.util.List;

public class OtherServicesDto {
    public List<String> outrosServicos;

    public OtherServicesDto(List<String> otherServices) {
        this.outrosServicos = otherServices;
    }
}
