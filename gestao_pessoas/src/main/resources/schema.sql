DROP TABLE IF EXISTS supplier;
DROP SEQUENCE IF EXISTS hibernate_sequence;

create table supplier (id integer not null, description varchar(255), primary key (id));
create sequence hibernate_sequence start with 1 increment by 1;