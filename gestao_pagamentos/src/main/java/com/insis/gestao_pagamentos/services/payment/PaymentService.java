package com.insis.gestao_pagamentos.services.payment;

import com.insis.gestao_pagamentos.interfaces.dto.activity.PaymentDto;
import com.insis.gestao_pagamentos.models.payment.Payment;

import java.util.List;

public interface PaymentService {

    Payment createPayment();

    Payment updatePayment(Payment payment);

    void deletePayment(int id);

    List<PaymentDto> getPayments();
}
