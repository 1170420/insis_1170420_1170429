package com.insis.gestao_pagamentos.services.payment;

import com.insis.gestao_pagamentos.interfaces.dto.activity.PaymentDto;
import com.insis.gestao_pagamentos.models.payment.Payment;
import com.insis.gestao_pagamentos.persistence.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {
    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public Payment createPayment() {
        return paymentRepository.save(new Payment("Success"));
    }

    @Override
    public Payment updatePayment(Payment payment) {
        Payment oldPayment;

        Optional<Payment> optionalPayment = paymentRepository.findAll()
                .stream().filter(a -> a.equals(payment))
                .findFirst();
        if (optionalPayment.isPresent()) {
            oldPayment = optionalPayment.get();
            oldPayment.setResult(payment.getResult());
            paymentRepository.save(oldPayment);
        } else {
            return new Payment();
        }
        return oldPayment;
    }

    @Override
    public void deletePayment(int id) {
        paymentRepository.deleteById(id);
    }

    public List<PaymentDto> getPayments() {
        List<String> payments = paymentRepository.findAll().stream().map(Payment::toString).collect(Collectors.toList());
        return payments.stream().map(PaymentDto::new).collect(Collectors.toList());
    }
}
