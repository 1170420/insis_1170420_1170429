package com.insis.gestao_pagamentos.models.payment;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "PAYMENT")
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "result")
    private String result;

    public Payment(String result) {
        this.result = result;
    }

    public Payment() {

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Payment)) {
            return false;
        }

        final Payment other = (Payment) obj;
        return getResult().equals(other.getResult());
    }

    public String getResult() {
        return result;
    }

    public void setResult(String date) {
        this.result = date;
    }

    @Override
    public int hashCode() {
        return Objects.hash(result);
    }

    @Override
    public String toString() {
        return result;
    }
}
