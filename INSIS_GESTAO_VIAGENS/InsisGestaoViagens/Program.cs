using Microsoft.EntityFrameworkCore;
using InsisGestaoViagens.Models;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<ViagemContext>(opt =>
    opt.UseInMemoryDatabase("ViagemList"));


builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "API_Gestao_Viagens", Description = "API Doc", Version = "v1" });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "API_Gestao_Viagens");
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
