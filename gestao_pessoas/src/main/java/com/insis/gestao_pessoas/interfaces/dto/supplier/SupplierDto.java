package com.insis.gestao_pessoas.interfaces.dto.supplier;

public class SupplierDto {
    public String description;

    public SupplierDto(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
