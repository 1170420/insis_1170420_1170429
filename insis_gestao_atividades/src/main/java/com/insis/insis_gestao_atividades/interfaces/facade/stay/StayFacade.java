package com.insis.insis_gestao_atividades.interfaces.facade.stay;

import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServiceDto;
import com.insis.insis_gestao_atividades.interfaces.dto.stay.StayDto;
import com.insis.insis_gestao_atividades.interfaces.dto.stay.StaysDto;

import java.util.List;

public interface StayFacade {

    StayDto createStay(StayDto stayDto);

    StayDto updateStay(StayDto stayDto);

    void deleteStay(int id);

    PayloadDto<StaysDto> getStays();

}
