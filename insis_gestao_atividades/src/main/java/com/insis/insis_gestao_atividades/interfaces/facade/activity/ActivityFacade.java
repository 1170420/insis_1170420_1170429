package com.insis.insis_gestao_atividades.interfaces.facade.activity;

import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.activity.ActivitiesDto;
import com.insis.insis_gestao_atividades.interfaces.dto.activity.ActivityDto;

import java.util.List;

public interface ActivityFacade {

    ActivityDto createActivity(ActivityDto activityDto);

    ActivityDto updateActivity(ActivityDto activityDto);

    void deleteActivity(int id);

    PayloadDto<ActivitiesDto> getActivities();

}
