package com.insis.insis_gestao_atividades.infrastructure.persistence;

import com.insis.insis_gestao_atividades.domain.models.activity.Activity;
import com.insis.insis_gestao_atividades.domain.models.trip.Trip;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {
}
