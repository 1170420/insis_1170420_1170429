package com.insis.insis_gestao_atividades.interfaces.web;

import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.trip.TripDto;
import com.insis.insis_gestao_atividades.interfaces.dto.trip.TripDto;
import com.insis.insis_gestao_atividades.interfaces.dto.trip.TripsDto;
import com.insis.insis_gestao_atividades.interfaces.facade.trip.TripFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/servicos/viagens")
public class TripsController {

    @Autowired
    private TripFacade tripFacade;

    @GetMapping("/obterViagens")
    public PayloadDto<TripsDto> getAllTrips() {
        return tripFacade.getTrips();
    }

    @PostMapping("/criarViagem")
    public TripDto addTrip(@RequestBody TripDto trip) {
        return tripFacade.createTrip(trip);
    }

    @PutMapping("/atualizarViagem")
    public TripDto updateTrip(@RequestBody TripDto trip) {
        return tripFacade.updateTrip(trip);
    }

    @DeleteMapping("/apagarViagem/{id}")
    public void deleteOutrosServicos(@PathVariable int id) {
        tripFacade.deleteTrip(id);
    }
}
