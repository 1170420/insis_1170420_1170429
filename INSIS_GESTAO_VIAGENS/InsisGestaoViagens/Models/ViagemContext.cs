using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using InsisGestaoViagens.Domain;

namespace InsisGestaoViagens.Models
{
    public class ViagemContext : DbContext
    {
        public ViagemContext(DbContextOptions<ViagemContext> options)
            : base(options)
        {
        }

        public DbSet<Viagem> Viagens { get; set; } = null!;
    }
}