DROP TABLE IF EXISTS activity;
DROP TABLE IF EXISTS other_service;
DROP TABLE IF EXISTS stay;
DROP TABLE IF EXISTS trip;
DROP SEQUENCE IF EXISTS hibernate_sequence;

create table activity (id integer not null, description varchar(255), minimum_age integer, primary key (id));
create table other_service (id integer not null, description varchar(255), primary key (id));
create table stay (id integer not null, description varchar(255), price integer, primary key (id));
create table trip (id integer not null, description varchar(255), price integer, primary key (id));
create sequence hibernate_sequence start with 1 increment by 1;