package com.insis.insis_gestao_atividades.application.services.trip;

import com.insis.insis_gestao_atividades.domain.models.trip.Trip;
import com.insis.insis_gestao_atividades.interfaces.dto.trip.TripsDto;

public interface TripService {

    Trip createTrip(Trip trip);

    Trip updateTrip(Trip trip);

    void deleteTrip(int id);

    TripsDto getTrips();
}
