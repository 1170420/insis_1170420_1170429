package com.insis.insis_gestao_atividades.interfaces.dto.other_service;

public class OtherServiceDto {
    public String description;

    public OtherServiceDto(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
