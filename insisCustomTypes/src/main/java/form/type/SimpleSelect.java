package form.type;

import org.activiti.engine.form.AbstractFormType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@SuppressWarnings("unchecked")
public class SimpleSelect extends AbstractFormType {

    private static final long serialVersionUID = 101L;

    public static final String TYPE_NAME = "simpleSelect";

    private static final Log LOGGER = LogFactory.getLog(SimpleSelect.class);

    public String getName() {
        return TYPE_NAME;
    }

    @Override
    public Object convertFormValueToModelValue(String propertyValue) {
        LOGGER.info("convert FORM VALUE to MODEL VALUE");
        LOGGER.info(propertyValue);

        return propertyValue;
    }

    @Override
    public String convertModelValueToFormValue(Object modelValue) {
        LOGGER.info("convert MODEL VALUE to FORM VALUE");

        if (modelValue == null) {
            LOGGER.info("object is null");
            return null;
        }

        LOGGER.info("Request data: " + modelValue.toString().subSequence(0, Math.min(100,  modelValue.toString().length())) + "...");

        return modelValue.toString();
    }
}
