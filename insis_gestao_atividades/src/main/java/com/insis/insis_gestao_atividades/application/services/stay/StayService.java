package com.insis.insis_gestao_atividades.application.services.stay;

import com.insis.insis_gestao_atividades.domain.models.other_service.OtherService;
import com.insis.insis_gestao_atividades.domain.models.stay.Stay;
import com.insis.insis_gestao_atividades.interfaces.dto.stay.StaysDto;

public interface StayService {

    Stay createStay(Stay stay);

    Stay updateStay(Stay stay);

    void deleteStay(int id);

    StaysDto getStays();
}
