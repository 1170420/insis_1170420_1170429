using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsisGestaoReservas.Domain;

public class Reserva
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public int ReservaId { get; set; }

        public String ReservaNome { get; set; }

        public Tipo ReservaTipo { get; set; }

        public Estado ReservaEstado { get;set; }

    }