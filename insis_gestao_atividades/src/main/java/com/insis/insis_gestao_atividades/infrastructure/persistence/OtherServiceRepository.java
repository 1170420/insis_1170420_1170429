package com.insis.insis_gestao_atividades.infrastructure.persistence;

import com.insis.insis_gestao_atividades.domain.models.activity.Activity;
import com.insis.insis_gestao_atividades.domain.models.other_service.OtherService;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OtherServiceRepository extends JpaRepository<OtherService, Integer> {
}
