package com.insis.gestao_pagamentos.interfaces.web;

import com.insis.gestao_pagamentos.interfaces.dto.activity.PaymentDto;
import com.insis.gestao_pagamentos.interfaces.facade.payment.PaymentFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pacote")
public class PaymentController {

    @Autowired
    private PaymentFacade paymentFacade;

    @GetMapping("/obterPagamentos")
    public List<PaymentDto> getAll() {
        return paymentFacade.getPayments();
    }

    @PostMapping("/pagarPacote")
    public PaymentDto addPayment() {
        return paymentFacade.createPayment();
    }

    @PutMapping("/atualizarPagamento")
    public PaymentDto updatePayment(@RequestBody PaymentDto payment) {
        return paymentFacade.updatePayment(payment);
    }

    @DeleteMapping("/apagarPagamento/{id}")
    public void deletePayment(@PathVariable int id) {
        paymentFacade.deletePayment(id);
    }
}
