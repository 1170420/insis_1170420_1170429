package com.insis.insis_gestao_atividades;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsisGestaoAtividadesApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsisGestaoAtividadesApplication.class, args);
    }
}
