package com.insis.insis_gestao_atividades.interfaces.dto.activity;

import java.util.List;

public class ActivitiesDto {
    public List<String> atividades;

    public ActivitiesDto(List<String> activities) {
        this.atividades = activities;
    }
}
