package com.insis.gestao_pagamentos.interfaces.facade.payment;

import com.insis.gestao_pagamentos.interfaces.dto.activity.PaymentDto;

import java.util.List;

public interface PaymentFacade {

    PaymentDto createPayment();

    PaymentDto updatePayment(PaymentDto paymentDto);

    void deletePayment(int id);

    List<PaymentDto> getPayments();

}
