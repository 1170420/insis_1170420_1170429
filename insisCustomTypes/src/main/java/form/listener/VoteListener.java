package form.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.TaskListener;

public class VoteListener implements TaskListener {

    private static final String SELECTED_ANSWER = "vote";
    private static final String RESULT = "approved";

    public Expression variableNameExpression;

    @Override
    @SuppressWarnings("unchecked")
    public void notify(DelegateTask delegateTask) {
        String vote = delegateTask.getVariable(SELECTED_ANSWER, String.class);
        Integer approved = delegateTask.getVariable(RESULT, Integer.class);

        System.out.println("listener string (vote): " + vote);

        if(vote != null && vote.equals("true")) {
            approved++;
            delegateTask.setVariable(RESULT, approved);
        }
    }
}
