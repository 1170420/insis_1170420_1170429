package com.insis.insis_gestao_atividades.domain.models.stay;

import javax.persistence.*;
import java.util.Objects;

@Entity(name="STAY")
public class Stay {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "description")
    private String description;
    @Column(name = "price")
    private int price;

    public Stay(String description, int price) {
        this.description = description;
        this.price = price;
    }

    public Stay() {

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Stay)) {
            return false;
        }

        final Stay other = (Stay) obj;
        return getDescription().equals(other.getDescription());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, price);
    }

    @Override
    public String toString() {
        return description + " - " + price;
    }
}
