package com.insis.insis_gestao_atividades.interfaces.web;

import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServiceDto;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServicesDto;
import com.insis.insis_gestao_atividades.interfaces.facade.other_service.OtherServiceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/servicos/outrosServicos")
public class OtherServiceController {

    @Autowired
    private OtherServiceFacade otherServiceFacade;

    @GetMapping("/obterOutrosServicos")
    public PayloadDto<OtherServicesDto> getAll() {
        return otherServiceFacade.getOtherservices();
    }

    @PostMapping("/criarOutrosServicos")
    public OtherServiceDto addOutrosServicos(@RequestBody OtherServiceDto otherServicesDto) {
        return otherServiceFacade.createOtherService(otherServicesDto);
    }

    @PutMapping("/atualizarOutrosServicos")
    public OtherServiceDto updateOutrosServicos(@RequestBody OtherServiceDto otherServiceDto) {
        return otherServiceFacade.updateOtherService(otherServiceDto);
    }

    @DeleteMapping("/apagarOutrosServicos/{id}")
    public void deleteOutrosServicos(@PathVariable int id) {
        otherServiceFacade.deleteOtherService(id);
    }
}
