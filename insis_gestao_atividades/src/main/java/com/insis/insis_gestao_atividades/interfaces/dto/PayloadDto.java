package com.insis.insis_gestao_atividades.interfaces.dto;

public class PayloadDto<T> {

    public T payload;

    public PayloadDto(T payload) {
        this.payload = payload;
    }

}
