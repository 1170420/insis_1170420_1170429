package com.insis.insis_gestao_atividades.interfaces.facade.trip;

import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.trip.TripDto;
import com.insis.insis_gestao_atividades.interfaces.dto.trip.TripsDto;

public interface TripFacade {

    TripDto createTrip(TripDto tripDto);

    TripDto updateTrip(TripDto tripDto);

    void deleteTrip(int id);

    PayloadDto<TripsDto> getTrips();

}
