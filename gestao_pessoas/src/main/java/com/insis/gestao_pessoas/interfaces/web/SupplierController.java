package com.insis.gestao_pessoas.interfaces.web;

import com.insis.gestao_pessoas.interfaces.dto.supplier.SupplierDto;
import com.insis.gestao_pessoas.interfaces.dto.supplier.SuppliersDto;
import com.insis.gestao_pessoas.interfaces.facade.supplier.SupplierFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pacote")
public class SupplierController {

    @Autowired
    private SupplierFacade supplierFacade;

    @GetMapping("/obterFornecedores")
    public SuppliersDto getAll() {
        return supplierFacade.getNumberOfSuppliers();
    }

    @PostMapping("/criarFornecedor")
    public SupplierDto addOutrosServicos(@RequestBody SupplierDto suppliersDto) {
        return supplierFacade.createSupplier(suppliersDto);
    }

    @PutMapping("/atualizarFornecedor")
    public SupplierDto updateOutrosServicos(@RequestBody SupplierDto supplierDto) {
        return supplierFacade.updateSupplier(supplierDto);
    }

    @DeleteMapping("/apagarFornecedor/{id}")
    public void deleteOutrosServicos(@PathVariable int id) {
        supplierFacade.deleteSupplier(id);
    }
}
