package com.insis.insis_gestao_atividades.application.services.activity;

import com.insis.insis_gestao_atividades.domain.models.activity.Activity;
import com.insis.insis_gestao_atividades.infrastructure.persistence.ActivityRepository;
import com.insis.insis_gestao_atividades.interfaces.dto.activity.ActivitiesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityRepository activityRepository;


    @Override
    public Activity createActivity(Activity activity) {
        return activityRepository.save(activity);
    }

    @Override
    public Activity updateActivity(Activity activity) {
        Activity oldActivity;

        Optional<Activity> optionalActivity = activityRepository.findAll()
                .stream().filter(a -> a.equals(activity))
                .findFirst();
        if (optionalActivity.isPresent()) {
            oldActivity = optionalActivity.get();
            oldActivity.setDescription(activity.getDescription());
            oldActivity.setMinimumAge(activity.getMinimumAge());
            activityRepository.save(oldActivity);
        } else {
            return new Activity();
        }
        return oldActivity;
    }

    @Override
    public void deleteActivity(int id) {
        activityRepository.deleteById(id);
    }

    public ActivitiesDto getActivities() {
        List<String> activities = activityRepository.findAll().stream().map(Activity::toString).collect(Collectors.toList());
        return new ActivitiesDto(activities);
    }
}
