package com.insis.gestao_pessoas.models.supplier;

import javax.persistence.*;
import java.util.Objects;

@Entity(name="SUPPLIER")
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "description")
    private String description;

    public Supplier(String description) {
        this.description = description;
    }

    public Supplier() {

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Supplier)) {
            return false;
        }

        final Supplier other = (Supplier) obj;
        return getDescription().equals(other.getDescription());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }

    @Override
    public String toString() {
        return description;
    }
}
