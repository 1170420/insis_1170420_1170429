package com.insis.insis_gestao_atividades.application.services.other_services;

import com.insis.insis_gestao_atividades.domain.models.other_service.OtherService;
import com.insis.insis_gestao_atividades.infrastructure.persistence.OtherServiceRepository;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServicesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class OtherServiceServiceImpl implements OtherServiceService {

    @Autowired
    private OtherServiceRepository otherServiceRepository;

    @Override
    public OtherService createOtherService(OtherService otherService) {
        return otherServiceRepository.save(otherService);
    }

    @Override
    public OtherService updateOtherService(OtherService otherService) {
        OtherService oldOtherService;

        Optional<OtherService> optionalOtherService = otherServiceRepository.findAll()
                .stream().filter(a -> a.equals(otherService))
                .findFirst();
        if (optionalOtherService.isPresent()) {
            oldOtherService = optionalOtherService.get();
            oldOtherService.setDescription(otherService.getDescription());
            otherServiceRepository.save(oldOtherService);
        } else {
            return new OtherService();
        }
        return oldOtherService;
    }

    @Override
    public void deleteOtherService(int id) {
        otherServiceRepository.deleteById(id);
    }

    @Override
    public OtherServicesDto getOtherServices() {
        List<String> otherServices = otherServiceRepository.findAll().stream().map(OtherService::toString).collect(Collectors.toList());
        return new OtherServicesDto(otherServices);
    }
}
