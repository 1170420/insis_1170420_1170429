using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InsisGestaoViagens.Domain;

public class Viagem
    {
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public int ViagemId { get; set; }

        public String ViagemNome { get; set; }

        public Double ViagemPreco { get; set; }
    }