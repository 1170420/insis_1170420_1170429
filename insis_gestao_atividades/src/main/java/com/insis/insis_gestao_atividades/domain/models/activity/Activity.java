package com.insis.insis_gestao_atividades.domain.models.activity;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "ACTIVITY")
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "description")
    private String description;
    @Column(name = "minimumAge")
    private int minimumAge;

    public Activity(String description, int minimumAge) {
        this.description = description;
        this.minimumAge = minimumAge;
    }

    public Activity() {

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Activity)) {
            return false;
        }

        final Activity other = (Activity) obj;
        return getDescription().equals(other.getDescription());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMinimumAge() {
        return minimumAge;
    }

    public void setMinimumAge(int minimumAge) {
        this.minimumAge = minimumAge;
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, minimumAge);
    }

    @Override
    public String toString() {
        return description + ": > " + minimumAge;
    }
}
