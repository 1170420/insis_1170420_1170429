package com.insis.gestao_pagamentos.interfaces.dto.activity;

public class PaymentDto {
    public String payment;

    public PaymentDto(String payment) {
        this.payment = payment;
    }

    @Override
    public String toString() {
        return payment;
    }
}
