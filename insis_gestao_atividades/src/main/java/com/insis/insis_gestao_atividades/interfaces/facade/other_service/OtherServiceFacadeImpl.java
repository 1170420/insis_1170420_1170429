package com.insis.insis_gestao_atividades.interfaces.facade.other_service;

import com.insis.insis_gestao_atividades.application.services.other_services.OtherServiceService;
import com.insis.insis_gestao_atividades.domain.models.other_service.OtherService;
import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServiceDto;
import com.insis.insis_gestao_atividades.interfaces.dto.other_service.OtherServicesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OtherServiceFacadeImpl implements OtherServiceFacade {

    @Autowired
    private OtherServiceService otherServiceService;

    @Override
    public OtherServiceDto createOtherService(OtherServiceDto otherServiceDto) {
        OtherService otherService = otherServiceService.createOtherService(new OtherService(otherServiceDto.description));
        return new OtherServiceDto(otherService.getDescription());
    }

    @Override
    public OtherServiceDto updateOtherService(OtherServiceDto otherServiceDto) {
        OtherService otherService = otherServiceService.updateOtherService(new OtherService(otherServiceDto.description));
        return new OtherServiceDto(otherService.getDescription());
    }

    @Override
    public void deleteOtherService(int id) {
        otherServiceService.deleteOtherService(id);
    }

    @Override
    public PayloadDto<OtherServicesDto> getOtherservices() {
        return new PayloadDto<>(otherServiceService.getOtherServices());
    }
}
