package com.insis.gestao_pessoas.interfaces.facade.supplier;

import com.insis.gestao_pessoas.interfaces.dto.supplier.SupplierDto;
import com.insis.gestao_pessoas.interfaces.dto.supplier.SuppliersDto;
import com.insis.gestao_pessoas.models.supplier.Supplier;
import com.insis.gestao_pessoas.services.supplier.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SupplierFacadeImpl implements SupplierFacade {

    @Autowired
    private SupplierService supplierService;

    @Override
    public SupplierDto createSupplier(SupplierDto supplierDto) {
        Supplier supplier = supplierService.createSupplier(new Supplier(supplierDto.description));
        return new SupplierDto(supplier.getDescription());
    }

    @Override
    public SupplierDto updateSupplier(SupplierDto supplierDto) {
        Supplier supplier = supplierService.updateSupplier(new Supplier(supplierDto.description));
        return new SupplierDto(supplier.getDescription());
    }

    @Override
    public void deleteSupplier(int id) {
        supplierService.deleteSupplier(id);
    }

    @Override
    public SuppliersDto getNumberOfSuppliers() {
        return supplierService.getNumberOfSuppliers();
    }
}
