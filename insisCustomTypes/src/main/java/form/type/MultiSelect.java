package form.type;

import org.activiti.engine.form.AbstractFormType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@SuppressWarnings("unchecked")
public class MultiSelect extends AbstractFormType {

    private static final long serialVersionUID = 100L;

    public static final String TYPE_NAME = "multiSelect";

    private static final Log LOGGER = LogFactory.getLog(MultiSelect.class);

    public String getName() {
        return TYPE_NAME;
    }

    @Override
    public Object convertFormValueToModelValue(String propertyValue) {
        LOGGER.info("propertyVal = " + propertyValue);
        return propertyValue;
    }

    @Override
    public String convertModelValueToFormValue(Object modelValue) {
        if (modelValue == null) {
            LOGGER.info("object is null");
            return null;
        }
        LOGGER.info("value = " + modelValue.toString());

        return modelValue.toString();
    }

}
