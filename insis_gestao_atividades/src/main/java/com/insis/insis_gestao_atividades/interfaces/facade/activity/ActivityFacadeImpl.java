package com.insis.insis_gestao_atividades.interfaces.facade.activity;

import com.insis.insis_gestao_atividades.application.services.activity.ActivityService;
import com.insis.insis_gestao_atividades.domain.models.activity.Activity;
import com.insis.insis_gestao_atividades.interfaces.dto.PayloadDto;
import com.insis.insis_gestao_atividades.interfaces.dto.activity.ActivitiesDto;
import com.insis.insis_gestao_atividades.interfaces.dto.activity.ActivityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityFacadeImpl implements ActivityFacade {

    @Autowired
    private ActivityService activityService;

    @Override
    public ActivityDto createActivity(ActivityDto activityDto) {
        Activity activityCreated = activityService.createActivity(new Activity(activityDto.description, activityDto.minimumAge));
        return new ActivityDto(activityCreated.getDescription(), activityCreated.getMinimumAge());
    }

    @Override
    public ActivityDto updateActivity(ActivityDto activityDto) {
        Activity updatedActivity = activityService.updateActivity(new Activity(activityDto.description, activityDto.minimumAge));
        return new ActivityDto(updatedActivity.getDescription(), updatedActivity.getMinimumAge());
    }

    @Override
    public void deleteActivity(int id) {
        activityService.deleteActivity(id);
    }

    @Override
    public PayloadDto<ActivitiesDto> getActivities() {
        return new PayloadDto<>(activityService.getActivities());
    }
}
