package com.insis.gestao_pessoas.interfaces.facade.supplier;


import com.insis.gestao_pessoas.interfaces.dto.supplier.SupplierDto;
import com.insis.gestao_pessoas.interfaces.dto.supplier.SuppliersDto;

public interface SupplierFacade {

    SupplierDto createSupplier(SupplierDto supplierDto);

    SupplierDto updateSupplier(SupplierDto supplierDto);

    void deleteSupplier(int id);

    SuppliersDto getNumberOfSuppliers();

}
