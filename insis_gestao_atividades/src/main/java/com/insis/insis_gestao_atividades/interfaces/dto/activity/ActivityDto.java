package com.insis.insis_gestao_atividades.interfaces.dto.activity;

public class ActivityDto {
    public String description;
    public int minimumAge;

    public ActivityDto(String description, int minimumAge) {
        this.description = description;
        this.minimumAge = minimumAge;
    }

    @Override
    public String toString() {
        return description;
    }
}
