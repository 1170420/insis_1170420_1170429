package com.insis.insis_gestao_atividades.application.services.activity;

import com.insis.insis_gestao_atividades.domain.models.activity.Activity;
import com.insis.insis_gestao_atividades.interfaces.dto.activity.ActivitiesDto;

public interface ActivityService {

    Activity createActivity(Activity activity);

    Activity updateActivity(Activity activity);

    void deleteActivity(int id);

    ActivitiesDto getActivities();
}
