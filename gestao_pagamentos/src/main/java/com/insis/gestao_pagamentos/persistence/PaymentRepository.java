package com.insis.gestao_pagamentos.persistence;

import com.insis.gestao_pagamentos.models.payment.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {
}
