using Microsoft.EntityFrameworkCore;
using InsisGestaoReservas.Domain;

namespace InsisGestaoReservas.Models
{
    public class ReservaContext : DbContext
    {
        public ReservaContext(DbContextOptions<ReservaContext> options)
            : base(options)
        {
        }

        public DbSet<Reserva> Reservas { get; set; } = null!;
    }
}