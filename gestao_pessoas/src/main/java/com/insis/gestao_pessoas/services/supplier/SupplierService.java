package com.insis.gestao_pessoas.services.supplier;

import com.insis.gestao_pessoas.interfaces.dto.supplier.SuppliersDto;
import com.insis.gestao_pessoas.models.supplier.Supplier;

public interface SupplierService {

    Supplier createSupplier(Supplier supplier);

    Supplier updateSupplier(Supplier Supplier);

    void deleteSupplier(int id);

    SuppliersDto getNumberOfSuppliers();
}
