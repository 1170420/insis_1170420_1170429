package com.insis.insis_gestao_atividades.domain.models.trip;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "TRIP")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "description")
    private String description;
    @Column(name = "price")
    private int price;

    public Trip(String description, int price) {
        this.description = description;
        this.price = price;
    }

    public Trip() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trip trip = (Trip) o;
        return getDescription().equals(trip.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, price);
    }

    @Override
    public String toString() {
        return description + " - " + price;
    }
}
