package com.insis.gestao_pessoas.persistence;

import com.insis.gestao_pessoas.models.supplier.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierRepository extends JpaRepository<Supplier, Integer> {
}
