package com.insis.gestao_pessoas.services.supplier;

import com.insis.gestao_pessoas.interfaces.dto.supplier.SuppliersDto;
import com.insis.gestao_pessoas.models.supplier.Supplier;
import com.insis.gestao_pessoas.persistence.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierRepository supplierRepository;

    @Override
    public Supplier createSupplier(Supplier supplier) {
        return supplierRepository.save(supplier);
    }

    @Override
    public Supplier updateSupplier(Supplier supplier) {
        Supplier oldSupplier;

        Optional<Supplier> optionalSupplier = supplierRepository.findAll()
                .stream().filter(a -> a.equals(supplier))
                .findFirst();
        if (optionalSupplier.isPresent()) {
            oldSupplier = optionalSupplier.get();
            oldSupplier.setDescription(supplier.getDescription());
            supplierRepository.save(oldSupplier);
        } else {
            return new Supplier();
        }
        return oldSupplier;
    }

    @Override
    public void deleteSupplier(int id) {
        supplierRepository.deleteById(id);
    }

    @Override
    public SuppliersDto getNumberOfSuppliers() {
        List<String> suppliers = supplierRepository.findAll().stream().map(Supplier::toString).collect(Collectors.toList());
        return new SuppliersDto(suppliers.size());
    }
}
