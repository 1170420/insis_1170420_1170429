package com.insis.gestao_pessoas.interfaces.dto.supplier;

public class SuppliersDto {
    public int numeroFornecedores;

    public SuppliersDto(int numSuppliers) {
        this.numeroFornecedores = numSuppliers;
    }

    public int getNumeroFornecedores() {
        return numeroFornecedores;
    }
}
