package com.insis.insis_gestao_atividades.infrastructure.persistence;

import com.insis.insis_gestao_atividades.domain.models.stay.Stay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StayRepository extends JpaRepository<Stay, Integer> {
}
